# **Purpose:**

This page is list of all the systems and tools we've evaluated, but ultimately decided to `Pass` on. 

For a list of the systems and tools in use now, please refer to the **Available Resources** table at the bottom of this page.

When evaluating a resource, please keep in mind the following:
   - What our current technology gaps are and what we're trying to solve for.
   - What we should implement to keep GitLab Recruiting on the leading edge of technology.  

Systems and Tools [Evaluation Committee.](https://gitlab.com/gitlab-com/people-ops/recruiting/blob/71029bfded80460e5c621f372a34d6b7afeddb3f/Recruitment%20Tools%20-%20Trials%20repository/evaluation_committee.md)

# **Sourcing Tools:**

[**Amazing Hiring**](https://gitlab.com/gitlab-com/people-ops/recruiting/issues/128)
* **Key Features:** Sourcing platform, contact finder, social platforms aggregator
* **Dates Evaluated:** 2019-04-16 to 2019-04-24
* **Evaluators:** Anastasia, Zsuzsanna
* **Vendor POC:** Asmik Sargsian; <as@amazinghiring.com>
* **Highlights:** Huge database, built-in contact finder
* **Lowlights:** Some profiles are outdated, no Greenhouse integration, mostly EMEA-focused, designed for tech sourcing only
* **Pricing:** $3,600 per seat
* **Decision:** `Pass`; price and search limitations

[**SeekOut**](https://gitlab.com/gitlab-com/people-ops/recruiting/issues/129)
* **Key Features:** Sourcing platform, email finder
* **Dates Evaluated:** 2019-04-16 to 2019-04-18
* **Evaluators:** Anastasia, Zsuzsanna
* **Vendor POC:** Jessica Fletcher; <jessica@seekout.io>
* **Highlights:** Diversity filter, "blind" hiring function (hides prospect's photo and name)
* **Lowlights:** Unfriendly UI, only US-based candidates, looks more like a LI parser than a great sourcing tool
* **Pricing:** Starting at $2,499 per seat
* **Decision:** `Pass`; see **Lowlights**

[**Resource.io**](https://gitlab.com/gitlab-com/people-ops/recruiting/issues/165)
* **Key Features:** Email finder, email tracker, email campaign generator
* **Dates Evaluated:** 2019-05-27 to 2019-06-30
* **Evaluators:** Alina, Anastasia, Catarina, Cyndi, Erich, Eva, Rupert
* **Vendor POC:** Monish Patel; monish@resource.io
* **Highlights:** Email nuture campaigns, integration with Google and LinkedIn
* **Lowlights:** Cumbersome UI, limited tracking capabilities, requires a lot of setup time
* **Pricing:** Starting at $5,000 per seat
* **Decision:** `Pass`; cost and time required to setup campaigns 

**Lusha** - No issue available
* **Key Features:** Contact finder that mainly focuses on phone numbers
* **Dates Evaluated:**
* **Evaluators:**
* **Vendor POC:**
* **Highlights:**
* **Lowlights:**
* **Pricing:**
* **Decision:** `Pass` [Reason for Pass]

[**Entelo**](https://gitlab.com/gitlab-com/people-ops/recruiting/issues/177)
* **Key Features:** Recruiting contact automation, sourcing platform
* **Dates Evaluated:** 2019-07-22 to
* **Evaluators:** Alina, Anastasia, Chris, Erich, Kanwal, Viren
* **Vendor POC:** Ashley O'Lear; aolear@entelo.com
* **Highlights:** UI, works globally, for tech- and non-tech roles, nuture campaigns, integration, search filters
* **Lowlights:** They don't offer a trial period, cost
* **Pricing:** $49,000 for 7 seats
* **Decision:** `Pass`; cost (ROI concerns)

**Teamable** - *Issue to be created*
* **Key Features:** Sourcing platform, connected to Team Member public networks
* **Dates Evaluated:** 2019-07-23 to
* **Evaluators:** Erich, Kevin
* **Vendor POC:** Bianca Djilas; bianca@teamable.com
* **Highlights:** Can ask Team Members for warm introductions
* **Lowlights:**
* **Pricing:**
* **Decision:** `Under Consideration`

[**Mixmax**](https://gitlab.com/gitlab-com/people-ops/recruiting/issues/164)
* **What:** All-in-one email tracking and meeting scheduling tool
* **Dates Evaluated:** 2019-05-14 to 2019-07-11
* **Evaluators:** Alina, Anastasia, Catarina, Erich, Eva, Rupert
* **Vendor POC:** Katie Helton; katie@mixmax.com
* **Highlights:**  Email tracking (clicks, open rates, and view details), intuitive native scheduling feature
* **Lowlights:** Privacy concerns, troublesome Greenhouse integration
* **Pricing:** n/a
* **Decision:** `Pass`' Privacy concerns and Greenhouse integration

# **Applicant Tracking Systems:**

**Google Hire** - No issue available
* **What:** Applicant Tracking System
* **Dates Evaluated:** 2019-05-01
* **Evaluators:** Erich, Kevin
* **Vendor POC:** Aalok Patel; aalokpatel@google.com
* **Highlights:** G-Suite integration, comprehensive platform (no upsells)
* **Lowlights:** Several quarters behind Greenhouse in terms of functionality
* **Pricing:** n/a
* **Decision:** `Pass`; Greenhouse is a superior ATS at the moment

# **Job Boards:**
- n/a

# **Available Resources:**

| **Resource**         | **Business Purpose**      | **Internal Contact** |
| -------------------- | ------------------------- | -------------------- |
| Calendly             | Meeting Scheduling        | TBD                  |
| [ContactOut](https://gitlab.com/gitlab-com/people-ops/recruiting/blob/master/Recruitment%20Tools%20-%20Trials%20repository/current_resources.md) | Emails and Phone Numbers  | TBD                  |
| DocuSign             | Digital Signatures        | Erich W.             | 
| Greenhouse           | Applicant Tracking System | Erich W.             |
| LinkedIn             | Sourcing                  | Erich W.             |
| Zoom                 | Video Conferencing        | People Ops           |
