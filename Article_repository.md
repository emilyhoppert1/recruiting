# ARTICLE REPOSITORY

## Purpose

This is a resource for the recruiting team to have a curated list of the latest blogs and articles about life at GitLab and remote work. 
These articles can be shared with candidates or used to have deeper conversations with candidates about what it's like to work here.

# Blogs about working at GitLab, from our team members

- [Not all remote is created equal](https://about.gitlab.com/2019/09/04/not-all-remote-is-created-equal/)
- [Why we believe all-remote is for everyone](https://about.gitlab.com/2019/08/15/all-remote-is-for-everyone/) 
- [How I balance a baby, a career at GitLab, and the cultural expectations of motherhood](https://about.gitlab.com/2019/07/25/balancing-career-and-baby/) 
- [How to live your best remote life](https://about.gitlab.com/2019/07/09/tips-for-working-from-home-remote-work/) 
- [5 Things to keep in mind while working remotely with kids](https://about.gitlab.com/2019/08/08/remote-kids-part-four/)
- [How I work from anywhere (with good internet)](https://about.gitlab.com/2019/06/25/how-remote-work-at-gitlab-enables-location-independence/) 
- [5 things you might hear when meeting with GitLab’s CEO](https://about.gitlab.com/2019/06/28/five-things-you-hear-from-gitlab-ceo/) 
- [One year at GitLab](http://shedoesdatathings.com/post/1-year-at-gitlab/) 
- [What we learned at Contribute 2019](https://about.gitlab.com/2019/06/04/contribute-wrap-up/)
- [A day in the life of the “average” remote worker](https://about.gitlab.com/2019/06/18/day-in-the-life-remote-worker/) 
- [How we turned a dull weekly all-hands into a podcast](https://about.gitlab.com/2019/06/03/how-we-turned-40-person-meeting-into-a-podcast/)
- [Why GitLab pays local rates](https://about.gitlab.com/2019/02/28/why-we-pay-local-rates/) 
- [How remote work enables rapid innovation at GitLab](https://about.gitlab.com/2019/02/27/remote-enables-innovation/) 
- [The case for all-remote companies](https://about.gitlab.com/2018/10/18/the-case-for-all-remote-companies/) 
- [How to keep healthy communication habits in remote teams](https://medium.com/gitlab-magazine/how-to-keep-healthy-communication-habits-in-remote-teams-a19eca371952) 
- [GitLab + STEM Gems: Giving girls role models in tech](https://about.gitlab.com/2018/10/08/stem-gems-give-girls-role-models/) 

# Articles about GitLab

- [How Open Source Became The Default Business Model For Software](https://www.forbes.com/sites/forbestechcouncil/2018/07/16/how-open-source-became-the-default-business-model-for-software/#5b4745f74e72)
- [Why GitLab's approach could ignite broad DevOps adoption](https://www.techrepublic.com/article/why-gitlabs-approach-could-ignite-broad-devops-adoption/)
- [He Built A $1 Billion Business Where All 700 Employees Work Remotely](https://www.forbes.com/sites/alejandrocremades/2019/07/21/he-built-a-1-billion-business-where-all-700-employees-work-remotely/)
- [More Proof (If Any Were Needed) that Work-From-Home Beats Open Plan Office](https://www.inc.com/geoffrey-james/more-proof-if-any-were-needed-that-work-from-home-beats-open-plan-office.html) 
- [The office of the future is no office at all, says startup](https://www.wsj.com/articles/the-office-of-the-future-is-no-office-at-all-says-startup-11557912601)
- [8 secrets to GitLab’s all-remote culture](https://www.tfir.io/2019/05/15/8-secrets-of-gitlabs-remote-work-success/) 
- [GitLab eyes more public sector business](https://www.zdnet.com/article/gitlab-eyes-more-public-sector-business/) 
- [Remote work demands a supportive company culture](https://insights.dice.com/2019/01/22/remote-work-demands-supportive-company-culture/) 
- [5 Fun Strategies Companies Are Using to Make Remote Workers Feel Included](https://business.linkedin.com/talent-solutions/blog/employee-engagement/2019/strategies-companies-use-to-keep-remote-workers-feeling-included) 
- [Tech’s newest unicorn has employees in 45 countries and zero offices](https://qz.com/work/1394496/gitlab-techs-newest-unicorn-has-no-offices/) 
- [$1.1 billion GitLab hires two new executives as it takes on GitHub and prepares to go public in 2020](https://www.businessinsider.com/gitlab-cmo-cro-ipo-2019-2)

# Articles about remote work in general

- [More People Are Working Remotely. That Means Unexpected Guests On Video Calls.](https://www.wsj.com/articles/why-are-you-shirtless-when-remote-video-calls-go-wrong-11565280525)
- [How remote work can reduce stress and revitalize your mindset](https://thriveglobal.com/stories/how-remote-work-can-reduce-stress-and-revitalize-your-mindset/) 
- [Five ways to stay motivated when you work remote](https://www.forbes.com/sites/stephanieburns/2019/05/30/5-ways-to-stay-motivated-when-you-work-remote/#24eb95265bf8)
- [4 golden rules for living the laptop lifestyle](https://www.forbes.com/sites/stephanieburns/2019/06/14/4-golden-rules-for-living-the-laptop-lifestyle/#287cbf5d3f62)
- [Here’s why remote workers are more productive than in-house teams](https://www.forbes.com/sites/abdullahimuhammed/2019/05/21/heres-why-remote-workers-are-more-productive-than-in-house-teams/#612c4adc6f9f) 


