## Welcome to the GitLab Recruiting team!

The first step for any new GitLabber is to complete their [onboarding issue](https://gitlab.com/gitlab-com/people-ops/employment/issues).

### Interviewing Issue

If you are a Recruiter or will be interviewing candidates, you need to create and complete an [inteviewing training issue](https://gitlab.com/gitlab-com/people-ops/employment/blob/master/.gitlab/issue_templates/interviewing.md) by going to the [GitLab People Ops Employment issue tracker](https://gitlab.com/gitlab-com/people-ops/employment/issues) (which is also where your onboarding issue is held), click the green button that says ["New Issue"](https://gitlab.com/gitlab-com/people-ops/employment/issues/new) at the top right, click on `Title` and choose `Interviewing`. Follow the instructions on the first few lines that pop up, assign the issue to yourself and get started! If you get stuck at any point, reach out to someone on the recruiting team who can create this issue for you so you can dive in.

### Greenhouse

Our ATS is Greenhouse, which if you have never used before, you will soon become closely acquainted with! We have a handbook page dedicated to [Greenhouse information](https://about.gitlab.com/handbook/hiring/greenhouse/), which will be very useful to you and your hiring managers.

During your onboarding, you will receive an invitation from Greenhouse to create your account. When you log in to Greenhouse, choose "Sign in with Google," making sure you're already logged into your GitLab email account. You should be set up as a `Job Admin: Recruiting`, which you can verify by hovering over your name in the top right corner, clicking "Account Settings", and scrolling down to "Your Permissions". If you are not a Job Admin: Recruiting for all jobs, ping someone on your team to update.

There are some basic settings you should also configure in Greenhouse to ensure you are preppred for success. In your account settings, under "Personal Information", be sure to update your timezone so it is accurate.

In your interview training issue, there will be a link to a recruiting team Greenhouse training. It is highly recommended to watch this video. There are also [hiring manager Greenhouse trainings](https://drive.google.com/drive/u/1/folders/1IK3Wb3P9_u0akMx5ASG26cuehY_LeRe8) which will be very helpful. Please note all trainings are confidential as they contain sensitive information.

Greenhouse has several integrations with partners we use, which you can connect by going to the [integrations page](https://app2.greenhouse.io/integrations) in your Greenhouse account. Everyone should connect their [Google Calendar account](https://support.greenhouse.io/hc/en-us/articles/201140605-I-use-Google-Calendar-How-do-I-schedule-an-interview-), and Recruiters and Sourcers should connect their [LinkedIn account](https://support.greenhouse.io/hc/en-us/articles/115005678103-Enable-LinkedIn-Recruiter-System-Connect-RSC-) (steps 2 and 3). Coordinators will need to connect their [BambooHR account](https://support.greenhouse.io/hc/en-us/articles/201177624-I-use-BambooHR-What-does-the-integration-look-like-How-do-I-enable-it-) and [DocuSign account](https://support.greenhouse.io/hc/en-us/articles/205633569-How-does-the-new-DocuSign-Integration-work-What-has-changed-).

### Hiring Process

The [vacancy process](https://about.gitlab.com/handbook/hiring/vacancies/) is done within Greenhouse by hiring managers. They create the position, which goes through an approval process, and it is then assigned to a recruiter, typically based on [a standard recruiting alignment](https://about.gitlab.com/handbook/hiring/recruiting-alignment/). The recruiting team follows up with [some cleanup tasks](https://about.gitlab.com/handbook/hiring/vacancies/#recruiting-team-tasks) to ensure everything is good to go, and then we begin hiring.

We have a [typical GitLab hiring process](https://about.gitlab.com/handbook/hiring/interviewing/#typical-hiring-timeline) for all vacancies, but each vacancy may vary, which you can discuss with your hiring managers, and which is clarified on each vacancy description. We also have an internal [hiring processes repository](https://gitlab.com/gitlab-com/people-ops/hiring-processes/) which contains the specific hiring process for each role and is a great resource.

Before moving to offer with a candidate, we conduct [reference checks](https://about.gitlab.com/handbook/hiring/interviewing/#reference-check-process) (which should be done by the hiring manager, but the coordinators also assist with), and the recruiter discusses compensation packages with the hiring manager. If there is a compensation calculator for the role, the hiring manager should suggest a level and experience factor for the candidate and use that number as the base salary. [Stock options](https://about.gitlab.com/handbook/stock-options/) are also almost always offered, and the amount varies based on role/level, but it can be negotiated. We have [general benefits](https://about.gitlab.com/handbook/benefits/) for all team members, but certain benefits are only eligible for certain employees who are employed through entities. Learning about our [contracts and entities](https://about.gitlab.com/handbook/contracts/#how-to-use), as well as where [we can't hire](https://about.gitlab.com/jobs/faq/#country-hiring-guidelines) will be very helpful when working with candidates! Once you have confirmed the compensation package and offer details, the recruiter will create an [offer package](https://about.gitlab.com/handbook/hiring/interviewing/#offer-package) in Greenhouse which will go through an approval process. Once approved, either the hiring manager or recruiter will make a verbal offer, which should be immediately followed by an offer email containing all of the information. An official contract is then sent out for signature, usually by the coordinator.

### Meet the team

Set up some 1:1s with members of the recruiting team, particularly those you'll be working with everyday, such as the recruiter, coordinator, and/or sourcer that is aligned to your function or location, as defined in the recruiting alignment handbook page linked above. This will help you to understand who does what on the team.

It's also recommended to reach out to have a 1:1 with any hiring managers you'll be working with, so you can get to know them and their team better, and understand what they are looking for.

Finally, it's good to have a 1:1 with the People Business Partner for the division you're aligned with.
