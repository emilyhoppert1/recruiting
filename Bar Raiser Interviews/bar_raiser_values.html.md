---
layout: markdown_page
title: "Bar Raiser Values"
---

## Values

A **Bar Raiser Interviewer** is an Interviewer who strictly focuses on [GitLab's Values](https://about.gitlab.com/handbook/values/) by way of asking several of the following CREDIT questions. The intent is to look for general intellectual capacity and problem-solving capabilities, often through case-style interviews, to assess a Candidate on their overall values alignment and to roughly determine the likelihood of their long-term success at GitLab.

### Bar Raiser CREDIT Questions

#### Collaboration

1.  **Tell me about a time when you were communicating with someone and they did not understand you. What did you do?**
1.  **Describe a time you had to make a decision after receiving different opinions from the team. How did you handle the situation?**
1.  **Could you tell me about a time when you worked alongside someone who was struggling with their work?**
1.  Describe a time your team failed to complete a project on time. What would you do differently if you had the chance?
1.  What would you do if you had to work with a person you didn’t get along with?
1.  Tell us about an idea you started that involved collaboration with your colleagues that improved the business
1.  Tell us about an idea you came up with that required you to collaborate with your colleagues in order to have a positive impact on the business.
1.  Could you tell us about a time you used the product of your company in order to improve your knowledge and effectiveness?
1.  Tell me about a time your team disagreed with your decision or ideas? How did you handle the situation?
1.  Give an example of when you had to work with someone who was difficult to work with. How did you handle interactions with that person?
1.  In a project you have taken part in, can you describe the action you took that was aimed at making your collaborators' jobs easier

#### Results

1.  **How do you define what it means to be an efficient and reliable employee?**
1.  **Tell me about your biggest professional failure and what you learned from it.**
1.  **What qualities do you have that will allow you to excel in an environment that stresses results over hours worked?**
1.  Tell me about a time when you promised to handle something at work that was either very difficult and/or came at a demanding time.
1.  Give an example of how you work in a situation where you must prioritize and multitask without supervision.
1.  How do you react when faced with many hurdles while trying to achieve a goal? How do you overcome the hurdles?
1.  Describe a situation in which you were most effective in achieving a very ambitious goal.
1.  Describe your best example of taking initiative on something that needed to be done, even though it wasn’t really your responsibility.
1.  How do you measure success in your current/previous role?

#### Efficiency

1.  **Describe a time when you realized that an initiative that you were working on shouldn’t have been a priority. How did you handle the situation?**
1.  **Describe something you have done to make your job more efficient.**
1.  **What steps do you take to ensure your work is not being duplicated across your team or organization?**
1.  Could you tell me about a time you implemented a very simple solution to a problem?
1.  Could you tell me about a time when you had to make a decision, even though you felt you didn’t have all the information you needed?
1.  Could you please tell us about a time when you found yourself on a team that was spending a large amount of time on an issue with little to no progress?
1.  How do you maximize efficiency in the meetings you own?

#### Diversity

1.  **Tell me about a time you worked with a team member(s) who had a very different opinion or perspective than you.**
1.  **What do you see as the fundamental characteristics of organizations that create an inclusive environment?**
1.  **Could you tell me about a time when having a diverse team lead to a good outcome, or when having a more diverse team could have lead to a better outcome? (Good answers might include a situation where someone's different background/experiences allowed them to think of a solution that no one else did. Possibly having a deep understanding of the needs of a particular subset of customers, which allowed them to avoid potential problems with the product design. Or how in hindsight a problem could have been avoided if the team had considered the diversity of their customers (e.g., all the "AI" tools that don't work with people from minorities).**
1.  Could you tell me about a time when you saw or heard something that wasn’t respectful or inclusive in the workplace? How did you handle the situation?
1.  What have you done to enhance your knowledge/skills related to diversity? How have you demonstrated what you learned?
1.  What are some examples of ways that you have incorporated diverse or underrepresented populations into your planning or decision-making?
1.  Please share an example that demonstrates your respect for people and their differences; how have you worked to understand the perspectives of others?

#### Iteration

1.  **Describe a time where you quickly started and shared a first iteration or draft even though it wasn't a finished product. What was the impact of sharing early? Were you comfortable working that way?**
1.  **Tell me about a time where you reduced scope to make quick progress on something.**
1.  **Tell me about one of your greatest accomplishments that initially started as a small iteration.**
1.  Describe a time in which you had to adjust quickly to changes over which you had no control. What was the impact of the change on you? On your work or project?
1.  Tell me about a time when you identified a problem with a process and the steps you took to improve the problem?
1.  When you had extra time available at your last job, describe the ways you found to make your job more efficient.
1.  On a project you took part in, can you describe the stages it went through, and what part you played in that.

#### Transparency

1.  **Describe a time when you shared information about a project you were working on beyond your core team even though you may have been more comfortable keeping it to yourself. What would you have done differently?**
1.  **Could you tell me about a time when you may have lost inspiration/motivation? What did you do to get yourself (and your team) back on track?**
1.  **How do you approach gathering feedback on the work you're doing? What do you do with that feedback, even if you disagree with it?**
1.  What does transparency mean to you? What impact do you think transparency has in an organization?
1.  Could you tell me about a time you made a mistake at work?
1.  Could you tell me about a time you strongly disagreed with a decision? How did you handle the situation?
1.  What steps do you take to provide transparency around the work you are doing?
1.  In a project you took part in which had external stakeholders, can you describe how you interacted with them? "How might you want to change that in the future?" (Good candidates will discuss multiple kinds of communication (direct emails, mediated communication, CI dashboards, source-code repositories) and can discuss the relative merits.) For people with a contracting background this will include communication with clients).
1.  Can you describe a time where you felt you needed more information about how a different part of your organization worked? What did you do about this? (* Good candidates will favor increased information flow by default and access to information without needing to request access. Good candidates will mention structural solutions (open source, code review, wikis, handbooks, company time-off calendars with public availability)).

